import { Component, OnInit, ViewChild, AfterViewInit, Injectable, ChangeDetectorRef, ViewEncapsulation, Input } from '@angular/core';
import { SessionService } from '../service/session.service';

@Component({
    selector: 'app-view-profile',
    templateUrl: './view-profile.component.html',
    styleUrls: ['./view-profile.component.scss']
})



export class ViewProfileComponent implements OnInit {
    userid = "";
    constructor(private chRef: ChangeDetectorRef, private sessionService: SessionService) { }

    ngOnInit() {
        this.userid = this.sessionService.getSessionUser();
    }

}
