import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewProfileComponent } from './view-profile.component';

const routes: Routes = [
  {
    path: '',
     component: ViewProfileComponent,
    data: {
      title: 'View Profile'
    },
    
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewProfileRoutingModule { }

export const routedComponents = [ViewProfileComponent];