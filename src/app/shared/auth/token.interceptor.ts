import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SessionService } from '../../service/session.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(public auth: SessionService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        request = request.clone({
            setHeaders: {
                Authorization: `Bearer ${this.auth.getAppTocken()}`
            }
        });

        return next.handle(request);
    }
}