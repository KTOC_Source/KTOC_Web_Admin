import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { IdentityService } from '../../../service/identity.service';
import * as uuid from "uuid";
import { SessionService } from '../../../service/session.service';
import { ProfileService } from '../../../service/profile.service';
@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss']
})

export class LoginPageComponent {
    email = "";
    password = "";
    udid = "";
    valid = false;
    validationmsg = "";
    @ViewChild('f') loginForm: NgForm;

    constructor(private router: Router,
        private route: ActivatedRoute, private identityService: IdentityService,
        private sessionService: SessionService, private profileService: ProfileService) { }

    // On submit button click    
    onSubmit() {
        debugger;
        if (this.email == "") {
            this.valid = true;
            this.validationmsg = "Email is required."
            return;
        }
        if (this.password == "") {
            this.valid = true;
            this.validationmsg = "Password is required."
            return;
        }
        this.valid = false;
        this.udid = uuid.v1();
        this.identityService.login(this.email, this.password, this.udid).subscribe(result => {
            debugger;
            console.log(result);
            if (result.status == "Success") {
                this.profileService.getProfileStatus(result.userId).subscribe(res => {
                    if (res.status == "Success") {
                        let ktocrecu = {
                            recuruiterId: result.userId
                        };
                        localStorage.setItem("KtocRecuriterInfo", JSON.stringify(ktocrecu));
                    } else {
                        localStorage.setItem("KtocRecuriterInfo", undefined);
                    }
                });
                localStorage.setItem("KtocUserInfo", JSON.stringify(result));
                this.router.navigateByUrl('/dashboard/dashboard');
            } else {
                this.valid = true;
                this.validationmsg = "Invalid Email or Password."
                return;
            }
        });
    }
    // On Forgot password link click
    onForgotPassword() {
        this.router.navigate(['forgotpassword'], { relativeTo: this.route.parent });
    }
    // On registration link click
    onRegister() {
        this.router.navigate(['register'], { relativeTo: this.route.parent });
    }
}