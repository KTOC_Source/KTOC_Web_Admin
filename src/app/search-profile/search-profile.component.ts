import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { NgbPanelChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import * as tableData from '../shared/data/smart-data-table';
import { LocalDataSource } from 'ng2-smart-table';
import { NouiFormatter } from "ng2-nouislider";
import { FormGroup, FormBuilder } from "@angular/forms";
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ProfileService } from '../service/profile.service';
import * as _ from "underscore";
import { SessionService } from '../service/session.service';
declare const $: any;

@Component({
    selector: 'app-search-profile',
    templateUrl: './search-profile.component.html',
    styleUrls: ['./search-profile.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class SearchProfileComponent implements OnInit {
    acc: any;
    collegelist: string[] = [];
    deptlist: string[] = [];
    source: LocalDataSource;
    closeResult: string;
    data: any[];
    userid = "";
    constructor(private modalService: NgbModal, private profileService: ProfileService, private sessionService: SessionService) {
        //this.source = new LocalDataSource(tableData.searchFilterData); // create the source

    }
    searchfilter = searchfilter;

    public ngOnInit() {
        //this.getSearchUserdata();
        debugger;
        this.userid = this.sessionService.getSessionUser();
        this.profileService.searchUser().subscribe(res => {
            let result = res.userList;
            let obj = {};
            searchFilterData = [];
            if (res.status == "Success") {
                if (result.length > 0) {
                    for (var i = 0; i < result.length; i++) {
                        obj = {
                            id: '<a _ngcontent-c5="" class="primary" target="_blank" (click)="open(viewprofilecontent)">' + result[i].ktocid + '</a>',
                            introVideo: result[i].introvideo,
                            ktocCost: result[i].points,
                            dept: result[i].collegeinfo[0].stream,
                            college: result[i].collegeinfo[0].collegename,
                            Certification: result[i].certification
                        }
                        searchFilterData.push(obj);
                    }
                }
            }
            this.source = new LocalDataSource(searchFilterData);
            //College Multiple select box loading
            clg = [];
            searchFilterData.map(function (ele) {
                clg.push(ele.college);
            });
            this.collegelist = _.uniq(clg);
            //Dept Multiple select box loading
            dept = [];
            searchFilterData.map(function (ele) {
                dept.push(ele.dept);
            });
            this.deptlist = _.uniq(dept);
        });
    }

    // Keyboard Support
    public someKeyboard: number[] = [1, 6];


    // Keyboard Support Configuration
    public someKeyboardConfig: any = {
        behaviour: 'drag',
        connect: true,
        start: [0, 10],
        keyboard: true,
        step: 0.1,
        pageSteps: 10,  // number of page steps, defaults to 10
        range: {
            min: 0,
            max: 10
        },
        pips: {
            mode: 'count',
            density: 2,
            values: 6,
            stepped: true
        }
    };

    // And the listener code which asks the DataSource to filter the data:
    onSearch(query: string = '') {
        this.source.setFilter([
            // fields we want to inclue in the search
            {
                field: 'id',
                search: query,
            },
            {
                field: 'name',
                search: query,
            },
            {
                field: 'username',
                search: query,
            },
            {
                field: 'email',
                search: query,
            },
        ], false);
        // second parameter specifying whether to perform 'AND' or 'OR' search
        // (meaning all columns should contain search query or at least one)
        // 'AND' by default, so changing to 'OR' by setting false here
    }

    // Open default modal
    open(content) {
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    // This function is used in open
    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    // Prevent panel toggle code
    public beforeChange($event: NgbPanelChangeEvent) {
        if ($event.panelId === '2') {
            $event.preventDefault();
        }
        if ($event.panelId === '3' && $event.nextState === false) {
            $event.preventDefault();
        }
    };

    public filterClgData(event): void {  // event will give you full breif of action
        const clgName = event.target.value;
        var filteredData = _.filter(searchFilterData, function (obj) { return obj.college == clgName });
        this.source = new LocalDataSource(filteredData);
    }

    public filterDeptData(event): void {  // event will give you full breif of action
        const deptName = event.target.value;
        var filteredData = _.filter(searchFilterData, function (obj) { return obj.dept == deptName });
        this.source = new LocalDataSource(filteredData);
    }

    public filterSearchField(event): void {  // event will give you full breif of action
        debugger;
        const fieldName = event.target.value.toLowerCase();
        //var filteredData =  _.filter(searchFilterData, function(obj) { return obj });
        let filteredList = [];
        var searchKeyword = fieldName.toLowerCase();
        searchFilterData.forEach(item => {
            //Object.values(item) => gives the list of all the property values of the 'item' object

            let propValueList = Object.keys(item).map(key => item[key]);
            for (let i = 0; i < propValueList.length; i++) {
                if (propValueList[i]) {
                    if (propValueList[i].toString().toLowerCase().indexOf(searchKeyword) > -1) {
                        filteredList.push(item);
                        break;
                    }
                }
            }
        });

        this.source = new LocalDataSource(filteredList);
    }

    public clearFilter(): void {
        this.source = new LocalDataSource(searchFilterData);
    }

}




// Search Profile Data Filter
export var searchfilter = {
    columns: {
        id: {
            title: 'KTOC ID',
            type: 'html',
            filter: false,
        },
        introVideo: {
            title: 'Intro Video',
            type: 'html',
            filter: false,
        },
        ktocCost: {
            title: 'KTOC Coin',
            filter: false,
        },
        dept: {
            title: 'Department',
            filter: false,
        },
        college: {
            title: 'College',
            filter: false,
        },
        Certification: {
            title: 'Certification',
            filter: false,
        }
    },
    attr: {
        class: "table table-responsive"
    },
    actions: false
};

export var searchFilterData = [
];

export var clg = [
];

export var dept = [
];

export var searchFilterData1 = [
    {
        id: 1,
        introVideo: '<a _ngcontent-c5="" class="primary" target="_blank" href="https://www.youtube.com/watch?v=R4CbD5X8jno"><img _ngcontent-c5="" class="width-50" height="50px" src="./assets/img/videoth/1.png" width="100px"><span _ngcontent-c5="" _ngcontent-c7="" class="fa ft-play-circle" (click)="open(VideoContent)"></span> Play</a>',
        ktocCost: '1,21,020',
        dept: 'CSE',
        college: 'VIT',
        Certification: 'Professional'
    },
    {
        id: 2,
        introVideo: '<a _ngcontent-c5="" class="primary" target="_blank" href="https://www.youtube.com/watch?v=R4CbD5X8jno"><img _ngcontent-c5="" class="width-50" height="50px" src="./assets/img/videoth/2.jpg" width="100px"><span _ngcontent-c5="" _ngcontent-c7="" class="fa ft-play-circle" (click)="open(VideoContent)"></span> Play</a>',
        ktocCost: '1,41,300',
        dept: 'CSE',
        college: 'VIT',
        Certification: 'Professional'
    },
    {
        id: 3,
        introVideo: '<a _ngcontent-c5="" class="primary" target="_blank" href="https://www.youtube.com/watch?v=R4CbD5X8jno"><img _ngcontent-c5="" class="width-50" height="50px" src="./assets/img/videoth/3.jpg" width="100px"><span _ngcontent-c5="" _ngcontent-c7="" class="fa ft-play-circle" (click)="open(VideoContent)"></span> Play</a>',
        ktocCost: '1,11,000',
        dept: 'CSE',
        college: 'VIT',
        Certification: 'Professional'
    },
    {
        id: 4,
        introVideo: '<a _ngcontent-c5="" class="primary" target="_blank" href="https://www.youtube.com/watch?v=R4CbD5X8jno"><img _ngcontent-c5="" class="width-50" height="50px" src="./assets/img/videoth/4.png" width="100px"><span _ngcontent-c5="" _ngcontent-c7="" class="fa ft-play-circle" (click)="open(VideoContent)"></span> Play</a>',
        ktocCost: '3,000',
        dept: 'CSE',
        college: 'VIT',
        Certification: 'Professional'
    },
    {
        id: 5,
        introVideo: '<a _ngcontent-c5="" class="primary" target="_blank" href="https://www.youtube.com/watch?v=R4CbD5X8jno"><img _ngcontent-c5="" class="width-50" height="50px" src="./assets/img/videoth/1.png" width="100px"><span _ngcontent-c5="" _ngcontent-c7="" class="fa ft-play-circle" (click)="open(VideoContent)"></span> Play</a>',
        ktocCost: '1,000',
        dept: 'CSE',
        college: 'VIT',
        Certification: 'Professional'
    },
    {
        id: 6,
        introVideo: '<a _ngcontent-c5="" class="primary" target="_blank" href="https://www.youtube.com/watch?v=R4CbD5X8jno"><img _ngcontent-c5="" class="width-50" height="50px" src="./assets/img/videoth/2.jpg" width="100px"><span _ngcontent-c5="" _ngcontent-c7="" class="fa ft-play-circle" (click)="open(VideoContent)"></span> Play</a>',
        ktocCost: '1,21,222',
        dept: 'CSE',
        college: 'VIT',
        Certification: 'Professional'
    },
    {
        id: 7,
        introVideo: '<a _ngcontent-c5="" class="primary" target="_blank" href="https://www.youtube.com/watch?v=R4CbD5X8jno"><img _ngcontent-c5="" class="width-50" height="50px" src="./assets/img/videoth/3.jpg" width="100px"><span _ngcontent-c5="" _ngcontent-c7="" class="fa ft-play-circle" (click)="open(VideoContent)"></span> Play</a>',
        ktocCost: '1,33,000',
        dept: 'CSE',
        college: 'VIT',
        Certification: 'Professional'
    },
    {
        id: 8,
        introVideo: '<a _ngcontent-c5="" class="primary" target="_blank" href="https://www.youtube.com/watch?v=R4CbD5X8jno"><img _ngcontent-c5="" class="width-50" height="50px" src="./assets/img/videoth/4.png" width="100px"><span _ngcontent-c5="" _ngcontent-c7="" class="fa ft-play-circle" (click)="open(VideoContent)"></span> Play</a>',
        ktocCost: '2,21,000',
        dept: 'CSE',
        college: 'VIT',
        Certification: 'Professional'
    },
    {
        id: 9,
        introVideo: '<a _ngcontent-c5="" class="primary" target="_blank" href="https://www.youtube.com/watch?v=R4CbD5X8jno"><img _ngcontent-c5="" class="width-50" height="50px" src="./assets/img/videoth/1.png" width="100px"><span _ngcontent-c5="" _ngcontent-c7="" class="fa ft-play-circle" (click)="open(VideoContent)"></span> Play</a>',
        ktocCost: '1,21',
        dept: 'CSE',
        college: 'VIT',
        Certification: 'Professional'
    },
    {
        id: 10,
        introVideo: '<a _ngcontent-c5="" class="primary" target="_blank" href="https://www.youtube.com/watch?v=R4CbD5X8jno"><img _ngcontent-c5="" class="width-50" height="50px" src="./assets/img/videoth/2.jpg" width="100px"><span _ngcontent-c5="" _ngcontent-c7="" class="fa ft-play-circle" (click)="open(VideoContent)"></span> Play</a>',
        ktocCost: '1,21,000',
        dept: 'CSE',
        college: 'VIT',
        Certification: 'Professional'
    },
    {
        id: 11,
        introVideo: '<a _ngcontent-c5="" class="primary" target="_blank" href="https://www.youtube.com/watch?v=R4CbD5X8jno"><img _ngcontent-c5="" class="width-50" height="50px" src="./assets/img/videoth/3.jpg" width="100px"><span _ngcontent-c5="" _ngcontent-c7="" class="fa ft-play-circle" (click)="open(VideoContent)"></span> Play</a>',
        ktocCost: '1,21,000',
        dept: 'CSE',
        college: 'VIT',
        Certification: 'Professional'
    },
    {
        id: 12,
        introVideo: '<a _ngcontent-c5="" class="primary" target="_blank" href="https://www.youtube.com/watch?v=R4CbD5X8jno"><img _ngcontent-c5="" class="width-50" height="50px" src="./assets/img/videoth/4.png" width="100px"><span _ngcontent-c5="" _ngcontent-c7="" class="fa ft-play-circle" (click)="open(VideoContent)"></span> Play</a>',
        ktocCost: '1,21,000',
        dept: 'CSE',
        college: 'VIT',
        Certification: 'Professional'
    },
];

