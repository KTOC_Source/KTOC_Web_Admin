import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchProfileRoutingModule } from "./search-profile-routing.module";
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { SearchProfileComponent } from './search-profile.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NouisliderModule } from 'ng2-nouislider';

import { VgCoreModule } from 'videogular2/core';
import { VgControlsModule } from 'videogular2/controls';
import { VgOverlayPlayModule } from 'videogular2/overlay-play';
import { VgBufferingModule } from 'videogular2/buffering';
import { GenerateProfileModule } from '../generateprofile/generateprofile.module';

@NgModule({
  imports: [
    CommonModule,
    SearchProfileRoutingModule,
    Ng2SmartTableModule,
    NgbModule,
    NouisliderModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    GenerateProfileModule
  ],
  declarations: [
    SearchProfileComponent
  ]
})
export class SearchProfileModule { }
