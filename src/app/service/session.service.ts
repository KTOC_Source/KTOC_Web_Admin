import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, URLSearchParams, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
@Injectable()
export class SessionService {
    private _status: boolean;
    private _sessionUser: any;
    private _sessionRecruiter: any;
    private _refreshToken: string;
    private _apptoken: string;
    constructor(private http: HttpClient) {

    }

    setSessionUser(data) {
        localStorage.setItem('KtocUserInfo', data);
    }

    updateAppTocken(token: string) {

    }

    getRecruiterUser() {
        let session = localStorage.getItem('KtocRecuriterInfo');
        let parsed = JSON.parse(session);
        if (parsed != undefined) {
            this._sessionRecruiter = parsed.recuruiterId;
            return this._sessionRecruiter;
        } else {
            this._sessionRecruiter = "";
            return "";
        }
    }

    getSessionUser() {
        let session = localStorage.getItem('KtocUserInfo');
        let parsed = JSON.parse(session);
        this._sessionUser = parsed.userId;
        return this._sessionUser;
    }
    getRefreshToken() {
        let session = localStorage.getItem('KtocUserInfo');
        let parsed = JSON.parse(session);
        this._refreshToken = parsed.refreshToken;
        return this._refreshToken;
    }

    isLoggedIn() {
        let currentUser = localStorage.getItem('KtocUserInfo');
        let user = JSON.parse(currentUser);
        if (user !== null) {
            this._status = true;
        }
        return this._status;
    }

    logOut() {
        this._status = !this._status;
    }

    getAppTocken() {
        let session = localStorage.getItem('KtocUserInfo');
        let parsed = JSON.parse(session);
        this._apptoken = parsed.sessionToken;
        return this._apptoken;
    }
}
