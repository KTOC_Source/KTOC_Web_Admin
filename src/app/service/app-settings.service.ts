import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppSettingsService {
  profileApiEndPoint = "";
  identityApiEndPoint = "";
  contentApiEndPoint = "";
  kingdomApiEndPoint = "";
  constructor() {
    this.identityApiEndPoint = "http://localhost:1000/api/identity/";
    this.profileApiEndPoint = "http://localhost:2000/api/profile/";
    this.contentApiEndPoint = "http://localhost:4000/api/content/";
    this.kingdomApiEndPoint = "http://ktoc-all-services-728310347.ap-south-1.elb.amazonaws.com:5000/api/kingdom/";
  }
}
