import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AppSettingsService } from './app-settings.service';
@Injectable({
  providedIn: 'root'
})
export class ContentService {

  constructor(private http: HttpClient, private appSettingsService: AppSettingsService) { }

  public getPublicDocumentList(filter: any): Observable<any> {
    let URI = this.appSettingsService.contentApiEndPoint + "document/getPublicDocumentList";
    return this.http.post(URI, filter)
      .map((res: Response) => {
        return res;
      })
      .catch((error: Response) => {
        return Observable.of(error);
      });
  }

  public getTodayKtocStar(): Observable<any> {
    let URI = this.appSettingsService.contentApiEndPoint + "getTodayKtocStar";
    return this.http.get(URI)
      .map((res: Response) => {
        return res;
      })
      .catch((error: Response) => {
        return Observable.of(error);
      });
  }

  public getAlltimeKtocStar(): Observable<any> {
    let URI = this.appSettingsService.contentApiEndPoint + "getAlltimeKtocStar";
    return this.http.get(URI)
      .map((res: Response) => {
        return res;
      })
      .catch((error: Response) => {
        return Observable.of(error);
      });
  }

  public getAlltimeKtocKingOrQueen(): Observable<any> {
    let URI = this.appSettingsService.kingdomApiEndPoint + "getAlltimeKtocKingOrQueen";
    return this.http.get(URI)
      .map((res: Response) => {
        return res;
      })
      .catch((error: Response) => {
        return Observable.of(error);
      });
  }

  public getKtocKingOrQueen(): Observable<any> {
    let URI = this.appSettingsService.kingdomApiEndPoint + "getKtocKingOrQueen";
    return this.http.get(URI)
      .map((res: Response) => {
        return res;
      })
      .catch((error: Response) => {
        return Observable.of(error);
      });
  }

  public getTopPlayer(): Observable<any> {
    let URI = this.appSettingsService.contentApiEndPoint + "getTopPlayer";
    return this.http.get(URI)
      .map((res: Response) => {
        return res;
      })
      .catch((error: Response) => {
        return Observable.of(error);
      });
  }

  public getHighValueResource(): Observable<any> {
    let URI = this.appSettingsService.contentApiEndPoint + "getHighValueResource";
    return this.http.get(URI)
      .map((res: Response) => {
        return res;
      })
      .catch((error: Response) => {
        return Observable.of(error);
      });
  }

}
