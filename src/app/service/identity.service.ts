import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { Http, Response, Headers } from '@angular/http';
import { AppSettingsService } from './app-settings.service';
@Injectable()
export class IdentityService {
  name: string;
  constructor(private http: Http, private appSettingsService: AppSettingsService) {

  }

  public login(loginid: string, password: string, udid: string): Observable<any> {
    let URI = this.appSettingsService.identityApiEndPoint + "login";
    return this.http.post(URI, { email: loginid, password: password, udid: udid })
      .map((res: Response) => {
        return res.json();
      })
      .catch((error: Response) => {
        return Observable.of(error.json());
      });
  }
}
