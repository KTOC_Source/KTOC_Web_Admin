import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AppSettingsService } from './app-settings.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  result: any;
  constructor(private http: HttpClient, private appSettingsService: AppSettingsService) { }

  sendSMSNotification(phoneNumber, msgBody) {
    var apiURL = "apiURL";
    const uri = apiURL + '/notification/api/sendsmsnotification/' + phoneNumber;

    return this
      .http
      .get(uri)
      .map(res => {

        return res;
      });
  }


  public getUser(userid: string): Observable<any> {
    let URI = this.appSettingsService.profileApiEndPoint + "getUser?userId=" + userid;
    return this.http.get(URI)
      .map((res: Response) => {
        return res;
      })
      .catch((error: Response) => {
        return Observable.of(error);
      });
  }

  public getKeyPercentageByUserId(userid: string): Observable<any> {
    let URI = this.appSettingsService.profileApiEndPoint + "getKeyPercentageByUserId?userId=" + userid;
    return this.http.get(URI)
      .map((res: Response) => {
        return res;
      })
      .catch((error: Response) => {
        return Observable.of(error);
      });
  }

  public getTagPercentageByUserId(userid: string): Observable<any> {
    let URI = this.appSettingsService.profileApiEndPoint + "getTagPercentageByUserId?userId=" + userid;
    return this.http.get(URI)
      .map((res: Response) => {
        return res;
      })
      .catch((error: Response) => {
        return Observable.of(error);
      });
  }

  public searchUser(): Observable<any> {
    let URI = this.appSettingsService.profileApiEndPoint + "searchUser";
    return this.http.get(URI)
      .map((res: Response) => {
        return res;
      })
      .catch((error: Response) => {
        return Observable.of(error);
      });
  }

  public getProfileStatus(recruiterId: string): Observable<any> {
    let URI = this.appSettingsService.profileApiEndPoint + "recruiter/getProfileStatus";
    let req = {
      recruiterId: recruiterId
    };
    return this.http.post(URI, req)
      .map((res: Response) => {
        return res;
      })
      .catch((error: Response) => {
        return Observable.of(error);
      });
  }

  public updateProfileStatus(reqdata: any): Observable<any> {
    let URI = this.appSettingsService.profileApiEndPoint + "recruiter/updateProfileStatus";
    return this.http.post(URI, reqdata)
      .map((res: Response) => {
        return res;
      })
      .catch((error: Response) => {
        return Observable.of(error);
      });
  }

  public getProfileStatusByUser(reqdata: any): Observable<any> {
    let URI = this.appSettingsService.profileApiEndPoint + "recruiter/getProfileStatusByUser";
    return this.http.post(URI, reqdata)
      .map((res: Response) => {
        return res;
      })
      .catch((error: Response) => {
        return Observable.of(error);
      });
  }
}
