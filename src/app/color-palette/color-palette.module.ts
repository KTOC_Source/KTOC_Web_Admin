import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { ColorPaletteRoutingModule } from "./color-palette-routing.module";

import { ColorPaletteComponent } from "./color-palette.component";
import { ViewProfileComponent } from './view-profile/view-profile.component';

@NgModule({
    imports: [
        CommonModule,
        ColorPaletteRoutingModule,
    ],
    declarations: [
        ColorPaletteComponent,
        ViewProfileComponent       
    ]
})
export class ColorPaletteModule { }
