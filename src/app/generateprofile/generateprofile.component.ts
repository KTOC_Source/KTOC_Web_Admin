import { Component, OnInit, ViewChild, AfterViewInit, Injectable, ChangeDetectorRef, ViewEncapsulation, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
declare const $: any;;
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import * as Chartist from 'chartist';
import { ChartType, ChartEvent } from "ng-chartist/dist/chartist.component";
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbPanelChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { NotificationService } from 'app/service/notificationService/notification.service';
import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl, FormControl } from '@angular/forms'
import { catchError, map, tap, startWith, switchMap, debounceTime, distinctUntilChanged, takeWhile, first } from 'rxjs/operators';
import { ProfileService } from '../service/profile.service';
import { SessionService } from '../service/session.service';
import * as _ from 'lodash';
import { ContentService } from '../service/content.service';

declare var require: any;

const data: any = require('../shared/data/chartist.json');

export interface Chart {
  type: ChartType;
  data: Chartist.IChartistData;
  options?: any;
  responsiveOptions?: any;
  events?: ChartEvent;
}

@Component({
  selector: 'app-generateprofile',
  templateUrl: './generateprofile.component.html',
  styleUrls: ['./generateprofile.component.scss']
})

export class GenerateprofileComponent implements OnInit {
  @Input() userId: string;
  acc: any;
  validationMessage: boolean;
  viewSMSForm: FormGroup;
  viewEmailForm: FormGroup;
  smsMSGBody: any;
  emailMSGBody: any;
  isSubmitted: boolean;
  userInfo: any;
  element: any;
  keyInfo: any;
  objectKeys = Object.keys;
  defaultimage = "./assets/img/portrait/avatars/default-avatar.png";
  peoplekey = "0";
  industrykey = "0";
  innovationkey = "0";
  entruponalkey = "0";
  curriculamkey = "0";
  activekey: any;
  totalcoins = "0";
  doucumentList: any;
  isRecuruiter = false;
  recuruiterId = "";
  isShortlisted = false;
  isViewed = false;
  isRejected = false;
  isRecruited = false;

  // Prevent panel toggle code
  public beforeChange($event: NgbPanelChangeEvent) {
    if ($event.panelId === '2') {
      $event.preventDefault();
    }
    if ($event.panelId === '3' && $event.nextState === false) {
      $event.preventDefault();
    }
  };
  closeResult: string;

  constructor(private chRef: ChangeDetectorRef, private http: HttpClient,
    private modalService: NgbModal, private frmBuilder: FormBuilder,
    private profileService: ProfileService, private sessionService: SessionService
    , private contentService: ContentService) { }

  ngOnInit() {
    this.recuruiterId = this.sessionService.getRecruiterUser();
    if (this.recuruiterId != "") {
      this.isRecuruiter = true;
      let statusReq = {
        recruiterId: this.recuruiterId,
        userId: this.userId
      };
      this.profileService.getProfileStatusByUser(statusReq).subscribe(recstaus => {
        if (recstaus.Viewed.length > 0) {
          this.isViewed = true;
        }
        if (recstaus.Shortlisted.length > 0) {
          this.isShortlisted = true;
        }
        if (recstaus.Recruited.length > 0) {
          this.isRecruited = true;
        }
        if (recstaus.Rejected.length > 0) {
          this.isRejected = true;
        }
      });
    } else {
      this.isRecuruiter = false;
    }
    this.getUser();
    this.viewSMSForm = this.frmBuilder.group({
      smsMSGBody: ["", [Validators.required]]
    });

    this.viewEmailForm = this.frmBuilder.group({
      emailMSGBody: ["", [Validators.required]]
    });



    $("#viewContainer div").on("click", function () {
      $("#viewContainer div").removeClass("active");
      $(this).addClass("active");
      // CALL scrollCenter PLUSGIN
      $("#viewContainer").scrollCenter(".active", 300);
    });

    $.fn.scrollCenter = function (elem, speed) {

      // this = #timepicker
      // elem = .active

      var active = jQuery(this).find(elem); // find the active element
      //var activeWidth = active.width(); // get active width
      var activeWidth = active.width() / 2; // get active width center

      //alert(activeWidth)

      //var pos = jQuery('#timepicker .active').position().left; //get left position of active li
      // var pos = jQuery(elem).position().left; //get left position of active li
      //var pos = jQuery(this).find(elem).position().left; //get left position of active li
      var pos = active.position().left + activeWidth; //get left position of active li + center position
      var elpos = jQuery(this).scrollLeft(); // get current scroll position
      var elW = jQuery(this).width(); //get div width
      //var divwidth = jQuery(elem).width(); //get div width
      pos = pos + elpos - elW / 2; // for center position if you want adjust then change this

      jQuery(this).animate({
        scrollLeft: pos
      }, speed == undefined ? 1000 : speed);
      return this;
    };

    // http://podzic.com/wp-content/plugins/podzic/include/js/podzic.js
    $.fn.scrollCenterORI = function (elem, speed) {
      jQuery(this).animate({
        scrollLeft: jQuery(this).scrollLeft() - jQuery(this).offset().left + jQuery(elem).offset().left
      }, speed == undefined ? 1000 : speed);
      return this;
    };
  }

  // Line chart configuration Ends

  shortlist() {
    if (this.recuruiterId != "") {
      let proreq = {
        recruiterId: this.recuruiterId,
        userId: this.userId,
        status: "Shortlisted",
        isActive: true
      };
      if (this.isShortlisted) {
        proreq.isActive = false;
        this.isShortlisted = false;
      } else {
        proreq.isActive = true;
        this.isShortlisted = true;
      }
      this.profileService.updateProfileStatus(proreq).subscribe(res => {

      });
    }
  }

  offered() {
    if (this.recuruiterId != "") {
      let proreq = {
        recruiterId: this.recuruiterId,
        userId: this.userId,
        status: "Recruited",
        isActive: true
      };
      if (this.isRecruited) {
        proreq.isActive = false;
        this.isRecruited = false;
      } else {
        proreq.isActive = true;
        this.isRecruited = true;
      }
      this.profileService.updateProfileStatus(proreq).subscribe(res => {

      });
    }
  }

  rejected() {
    if (this.recuruiterId != "") {
      let proreq = {
        recruiterId: this.recuruiterId,
        userId: this.userId,
        status: "Rejected",
        isActive: true
      };
      if (this.isRejected) {
        proreq.isActive = false;
        this.isRejected = false;
      } else {
        proreq.isActive = true;
        this.isRejected = true;
      }
      this.profileService.updateProfileStatus(proreq).subscribe(res => {

      });
    }
  }

  getUser() {
    this.userId = this.sessionService.getSessionUser();
    this.profileService.getUser(this.userId).subscribe(res => {
      this.userInfo = res.userInfo;
      this.totalcoins = res.totalcoins;
    });
    this.profileService.getKeyPercentageByUserId(this.userId).subscribe(res => {
      this.keyInfo = res.keyInfo;
      let people: any = _.filter(this.keyInfo, { 'keyname': "People Key" });
      this.peoplekey = people[0].percentage;
      let industry: any = _.filter(this.keyInfo, { 'keyname': "Industrial Key" });
      this.industrykey = industry[0].percentage;
      let innovation: any = _.filter(this.keyInfo, { 'keyname': "Innovator Key" });
      this.innovationkey = innovation[0].percentage;
      let entruponal: any = _.filter(this.keyInfo, { 'keyname': "Entrepreneur Key" });
      this.entruponalkey = entruponal[0].percentage;
      let curriculam: any = _.filter(this.keyInfo, { 'keyname': "Puvi Key" });
      this.curriculamkey = curriculam[0].percentage;
    });
    this.profileService.getTagPercentageByUserId(this.userId).subscribe(res => {
      let taginfo = res.tagInfo;
      let keyList = _.groupBy(taginfo, 'keyid');
      this.element = keyList;
      let keys = this.objectKeys(this.element)
      this.activekey = keys[0];
    });
    let filter = {
      userId: this.userId
    };
    this.contentService.getPublicDocumentList(filter).subscribe(res => {
      console.log(res);
      this.doucumentList = res.doucumentList;
    });
  }
  // Open default modal
  open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  //Send SMS
  sendSMSNotification() {
    debugger;
    var smsMsgBody = this.viewSMSForm.value.smsMSGBody;

    if (!this.viewSMSForm.valid) {
      this.isSubmitted = true;
      this.validationMessage = true;
      return;
    }
    var phoneNumber = "+919750622146";

    //TODO have to change with respect to API's
    // this.notificationService.sendSMSNotification(phoneNumber, smsMsgBody).subscribe(res => {
    //     debugger;
    // //this.NotificationResult = res;

    // });
  }


  //Send Email
  sendEmailNotification() {
    debugger;
    var emailMSGBody = this.viewEmailForm.value.emailMSGBody;

    if (!this.viewEmailForm.valid) {
      this.isSubmitted = true;
      this.validationMessage = true;
      return;
    }

    //TODO have to change with respect to API's
    // this.notificationService.sendSMSNotification(phoneNumber, smsMsgBody).subscribe(res => {
    //     debugger;
    // //this.NotificationResult = res;

    // });
  }


  // This function is used in open
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


}

