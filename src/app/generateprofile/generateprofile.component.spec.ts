import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateprofileComponent } from './generateprofile.component';

describe('GenerateprofileComponent', () => {
  let component: GenerateprofileComponent;
  let fixture: ComponentFixture<GenerateprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
