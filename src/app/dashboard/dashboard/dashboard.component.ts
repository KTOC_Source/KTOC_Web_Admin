import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import * as Chartist from 'chartist';
import { ChartType, ChartEvent } from "ng-chartist/dist/chartist.component";
declare var require: any;
import { ProfileService } from '../../service/profile.service';
import { SessionService } from '../../service/session.service';
import * as _ from 'lodash';
import { ContentService } from '../../service/content.service';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

const data: any = require('../../shared/data/chartist.json');
export interface Chart {
    type: ChartType;
    data: Chartist.IChartistData;
    options?: any;
    responsiveOptions?: any;
    events?: ChartEvent;
}

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})


export class DashboardComponent implements OnInit {
    // Line chart configuration Starts
    defaultimage = "./assets/img/portrait/avatars/default-avatar.png";
    ktocstarList: any;
    ktocalltimestarList: any;
    ktockingList: any;
    ktocalltimeking: any;
    ktocstaruser: any;
    ktocalltimestaruser: any;
    ktockinguser: any;
    ktocalltimekinguser: any;
    topplayers: any;
    ViewedList: any = [];
    ShortlistedList: any = [];
    RecruitedList: any = [];
    RejectedList: any = [];
    highvalueplayer: any;
    constructor(private chRef: ChangeDetectorRef, private modalService: NgbModal,
        private profileService: ProfileService, private sessionService: SessionService
        , private contentService: ContentService) { }

    ngOnInit() {
        this.ktocstar();
    }

    ktocstar() {
        let userId = this.sessionService.getSessionUser();
        this.contentService.getTodayKtocStar().subscribe(res => {
            this.ktocstarList = res.result
            if (this.ktocstarList) {
                this.profileService.getUser(this.ktocstarList.userid).subscribe(user => {
                    this.ktocstaruser = user.userInfo;
                });
            }
        });
        this.contentService.getAlltimeKtocStar().subscribe(res => {
            this.ktocalltimestarList = res.star;
            debugger;
            if (this.ktocalltimestarList) {
                this.profileService.getUser(this.ktocalltimestarList.userid).subscribe(user => {
                    this.ktocalltimestaruser = user.userInfo;
                });
            }
        });
        this.contentService.getKtocKingOrQueen().subscribe(res => {
            this.ktockingList = res.result;
            if (this.ktockingList) {
                this.profileService.getUser(this.ktockingList.userid).subscribe(user => {
                    this.ktockinguser = user.userInfo;
                });
            }
        });
        this.contentService.getAlltimeKtocKingOrQueen().subscribe(res => {
            this.ktocalltimeking = res.result;
            if (this.ktocalltimeking) {
                this.profileService.getUser(this.ktocalltimeking.userid).subscribe(user => {
                    this.ktocalltimekinguser = user.userInfo;
                });
            }
        });

        this.contentService.getTopPlayer().subscribe(res => {
            this.topplayers = res.players;
        });
        this.contentService.getHighValueResource().subscribe(res => {
            this.highvalueplayer = res.player;
        });
        this.profileService.getProfileStatus(userId).subscribe(res => {
            if (res.status == "Success") {
                this.ViewedList = res.Viewed;
                this.ShortlistedList = res.Shortlisted;
                this.RecruitedList = res.Recruited;
                this.RejectedList = res.Rejected;
            }
        });
    }
    // Donut chart configuration Starts
    DonutChart1: Chart = {
        type: 'Pie',
        data: data['DashboardDonut'],
        options: {
            donut: true,
            donutWidth: 3,
            startAngle: 0,
            chartPadding: 25,
            labelInterpolationFnc: function (value) {
                return '\ue9c9';
            }
        },
        events: {
            draw(data: any): void {
                if (data.type === 'label') {
                    if (data.index === 0) {
                        data.element.attr({
                            dx: data.element.root().width() / 2,
                            dy: (data.element.root().height() + (data.element.height() / 4)) / 2,
                            class: 'ct-label',
                            'font-family': 'feather'
                        });
                    } else {
                        data.element.remove();
                    }
                }
            }
        }
    };
    // Donut chart configuration Ends

    // Donut chart configuration Starts
    DonutChart2: Chart = {
        type: 'Pie',
        data: data['DashboardDonut'],
        options: {
            donut: true,
            donutWidth: 3,
            startAngle: 90,
            chartPadding: 25,
            labelInterpolationFnc: function (value) {
                return '\ue9e7';
            }
        },
        events: {
            draw(data: any): void {
                if (data.type === 'label') {
                    if (data.index === 0) {
                        data.element.attr({
                            dx: data.element.root().width() / 2,
                            dy: (data.element.root().height() + (data.element.height() / 4)) / 2,
                            class: 'ct-label',
                            'font-family': 'feather'
                        });
                    } else {
                        data.element.remove();
                    }
                }
            }
        }
    };
    // Donut chart configuration Ends

    // Donut chart configuration Starts
    DonutChart3: Chart = {
        type: 'Pie',
        data: data['DashboardDonut'],
        options: {
            donut: true,
            donutWidth: 3,
            startAngle: 270,
            chartPadding: 25,
            labelInterpolationFnc: function (value) {
                return '\ue964';
            }
        },
        events: {
            draw(data: any): void {
                if (data.type === 'label') {
                    if (data.index === 0) {
                        data.element.attr({
                            dx: data.element.root().width() / 2,
                            dy: (data.element.root().height() + (data.element.height() / 4)) / 2,
                            class: 'ct-label',
                            'font-family': 'feather'
                        });
                    } else {
                        data.element.remove();
                    }
                }
            }
        }
    };
    // Donut chart configuration Ends

    // Bar chart configuration Ends
}